import { Component, EventEmitter, Input, OnChanges, OnInit, Output, SimpleChanges } from '@angular/core';

@Component({
  selector: 'fg-level-indicator',
  templateUrl: './level-indicator.component.html',
  styleUrls: ['./level-indicator.component.scss']
})
export class LevelIndicatorComponent implements OnInit, OnChanges {

  levels: any[];

  @Input()
  maxLevel: number;

  @Input()
  level: number;

  @Input()
  currentLevel: number;

  @Output()
  levelChange = new EventEmitter<number>();

  constructor() {
    this.levels = [];
  }

  ngOnChanges(changes: SimpleChanges): void {
    this.levels = Array(changes.maxLevel || 0).fill(undefined);
  }

  ngOnInit(): void {

  }

  _onClickLeft(): void {
    console.log('left');
    if (this.currentLevel > 1) {
      this.currentLevel = this.currentLevel - 1;
    }
  }

  _onClickRight(): void {
    console.log('right');
    if (this.currentLevel <= 23) {
      this.currentLevel = this.currentLevel + 1;
    }
  }

}
